/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.text.service;

import net.coalevo.text.model.TransformationException;
import net.coalevo.text.model.UnsupportedInputFormatException;
import net.coalevo.text.model.UnsupportedOutputFormatException;
import net.coalevo.text.model.UnsupportedTransformException;

import java.util.Set;

/**
 * Defines an interface for a text transformation service.
 * <p/>
 * Note that a service implementation may implement indirect
 * transformations, using a chain of transformations to
 * convert from a valid input format to a valid output format.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface TransformationService {

  /**
   * Transforms a given String from a given format
   *
   * @param inf  input format specifier.
   * @param outf output format specifier.
   * @param text the text to be transformed.
   * @return the transformed text in output format.
   * @throws UnsupportedTransformException if the transformation is not supported.
   * @throws TransformationException       if the input is not valid within the
   *                                       input format specification.
   */
  public String transform(String inf, String outf, String text)
      throws UnsupportedTransformException, TransformationException;

  /**
   * Returns a set of string specifiers for all supported input formats.
   *
   * @return a <tt>Set</tt> of String specifiers denoting supported
   *         input formats.
   */
  public Set<String> getInputFormats();

  /**
   * Returns a set of string specifiers for all input formats
   * that may be transformed to a given output format.
   *
   * @param outf the output format specifier.
   * @return a <tt>Set</tt> of String specifiers denoting supported
   *         input formats.
   * @throws UnsupportedOutputFormatException
   *          if the given output format
   *          specifier does not map to a supported output format.
   */
  public Set<String> getInputFormats(String outf)
      throws UnsupportedOutputFormatException;

  /**
   * Returns a set of string specifiers for all supported output formats.
   *
   * @return a <tt>Set</tt> of String specifiers denoting supported
   *         output formats.
   */
  public Set<String> getOutputFormats();

  /**
   * Returns a set of string specifiers for all output formats
   * that may be transformed to a given input format.
   *
   * @param inf the input format specifier.
   * @return a <tt>Set</tt> of String specifiers denoting supported
   *         output formats.
   * @throws UnsupportedInputFormatException
   *          if the given input format
   *          specifier does not map to a supported input format.
   */
  public Set<String> getOutputFormats(String inf)
      throws UnsupportedInputFormatException;

}//interface TransformationService
