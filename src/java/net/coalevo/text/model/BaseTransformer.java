/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.text.model;

/**
 * Provides an abstract base class for {@link Transformer}
 * implementations.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public abstract class BaseTransformer
    implements Transformer {

  protected String m_Identifier;
  protected String m_InputFormat;
  protected String m_OutputFormat;
  protected boolean m_Direct = true;
  protected int m_NumberOfPasses = 1;

  public BaseTransformer(String inf, String outf) {
    m_InputFormat = inf;
    m_OutputFormat = outf;
    m_Identifier = new StringBuilder(inf).append("=>").append(outf).toString();
  }//BaseTransformer

  public boolean isDirect() {
    return m_Direct;
  }//isDirect

  public String getInputFormat() {
    return m_InputFormat;
  }//getFrom

  public String getOutputFormat() {
    return m_OutputFormat;
  }//getOutputFormat

  public int getNumberOfPasses() {
    return m_NumberOfPasses;
  }//getNumberOfPasses

  public String getIdentifier() {
    return m_Identifier;
  }//getIdentifier

  public String toString() {
    StringBuilder sb = new StringBuilder(super.toString());
    sb.append('[');
    sb.append(m_Identifier);
    sb.append(']');
    return sb.toString();
  }//toString


  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    BaseTransformer that = (BaseTransformer) o;

    if (m_Identifier != null ? !m_Identifier.equals(that.m_Identifier) : that.m_Identifier != null) return false;

    return true;
  }//equals

  public int hashCode() {
    return (m_Identifier != null ? m_Identifier.hashCode() : 0);
  }//hashCode

}//class BaseTransformer
