/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.text.model;

/**
 * This interface defines a transformer.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface Transformer {

  /**
   * Returns the input format specifier.
   *
   * @return the input format specifier string.
   */
  public String getInputFormat();

  /**
   * Returns the input format specifier.
   *
   * @return the input format specifier string.
   */
  public String getOutputFormat();

  /**
   * Transforms and returns the output.
   *
   * @param input the input string.
   * @return the transformed output.
   * @throws TransformationException if the transformation cannot be completed.
   */
  public String transform(String input)
      throws TransformationException;

  /**
   * Returns the number of passes required to transform from
   * the input to the output.
   * <p/>
   * Implementations should provide a real value for this, given
   * that it can be used to construct the shortest indirect path
   * to transform from one input format to an non-directly achievable
   * output format.
   * </p>
   *
   * @return the number of passes required to transform the input to the output.
   */
  public int getNumberOfPasses();

  /**
   * Tests if this transformation is direct or uses intermediate
   * transformations.
   *
   * @return true if direct, false otherwise.
   */
  public boolean isDirect();

  /**
   * Returns a unique identifier for this transformer, that
   * should be composed of input and output format, whether chained
   * or not.
   *
   * @return an identifier.
   */
  public String getIdentifier();

}//interface Transformer
