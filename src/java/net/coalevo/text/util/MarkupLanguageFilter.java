/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.text.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Provides an utility class that performs filtering of
 * Markup Language strings (e.g. XML tags and whitespace).
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class MarkupLanguageFilter {


  private MarkupLanguageFilter() {
  }

  ; //non instantiable

  /**
   * Filters Markup Language Tags from the given <tt>String</tt>.
   *
   * @param str the <tt>String</tt> to be filtered.
   * @return the <tt>String</tt> without Markup Language Tags.
   */
  public static String filterMLTags(String str) {
    return (str == null) ? EMPTY : str.replaceAll(ML_TAG, "");
  }//filterMLTags

  /**
   * Filters whitespace from the given <tt>String</tt>.
   *
   * @param str the <tt>String</tt> to be filtered.
   * @return the <tt>String</tt> without whitespace.
   */
  public static String filterWhitespace(String str) {
    return (str == null) ? EMPTY : str.replaceAll(WS, "");
  }//filterWhitespace

  public static String filterMLWS(String str) {
    if(str == null || str.length() == 0) {
      return str;
    }
    return str.replaceAll(WSML, "");
  }//filterMLWS

  public static String filterWS(String str) {
    if (str == null || str.equals("")) return str;
    str = str.replaceAll("\n", " ");
    str = str.replaceAll("(\\x20{2,})*", "");
    return str;
  }//filterWS

  public static String encodeMLSpecials(String str) {
    if (str == null) {
      return EMPTY;
    }
    Matcher m = m_Specials.matcher(str);
    StringBuffer sb = new StringBuffer();
    while (m.find()) {
      String g = m.group();
      if ("<".equals(g)) {
        m.appendReplacement(sb, "&lt;");
      } else if (">".equals(g)) {
        m.appendReplacement(sb, "&gt;");
      } else if ("&".equals(g)) {
        m.appendReplacement(sb, "&amp;");
      }
    }
    m.appendTail(sb);
    return sb.toString();
  }//encodeMLSpecials


  /**
   * Regular Expression for Markup Language Tags
   * (<tt>&lt;[^&gt;]*&gt;</tt>).
   */
  public static final String ML_TAG = "<[^>]*>";

  /**
   * Regular Expression for Whitespace
   * (<tt>(\n|\r|\t|((' '){2,}?))*</tt>).
   */
  public static final String WS = "(\n|\r|\t|\\x20{2,})*";

  /**
   * Regular Expression for Markup Language Tags and
   * Whitespace.
   */
  public static final String WSML = "(\n|\r|\t|\f|\\x20{2,}|<[^>]*>)*";

  /**
   * Regular Expression for Markup Language Special characters.
   */
  public static final String MLSPECIALS = "(<|>|&)";

  /**
   * Stores the compiled pattern for MLSPECIALS
   */
  private static Pattern m_Specials = Pattern.compile(MLSPECIALS);

  /**
   * An empty String.
   */
  private static String EMPTY = "";

}//class MarkupLanguageFilter
