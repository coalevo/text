/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.text.util;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * This class provides an LRU cache for string transforms
 * that are identifiable with a string key.
 * <p/>
 * Basically this cache wraps a LinkedHashMap<String,String>.
 * </p>
 * Todo: Extend for automatic transforming and caching, eg. transparent
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class TransformCache
    implements Map<String, String> {

  private Map<String, String> m_Cache;

  public TransformCache(int size) {
    m_Cache = new LinkedHashMap<String, String>(size, 0.75f, true);
  }//constructor

  public int size() {
    return m_Cache.size();
  }//size

  public boolean isEmpty() {
    return m_Cache.isEmpty();
  }//isEmpty

  public boolean containsKey(Object key) {
    return m_Cache.containsKey(key);
  }//containsKey

  public boolean containsValue(Object value) {
    return m_Cache.containsValue(value);
  }//containsValue

  public String get(Object key) {
    return m_Cache.get(key);
  }//get

  public String put(String key, String value) {
    return m_Cache.put(key, value);
  }//put

  public String remove(Object key) {
    return m_Cache.remove(key);
  }//remove

  public void putAll(Map<? extends String, ? extends String> t) {
    m_Cache.putAll(t);
  }//putAll

  public void clear() {
    m_Cache.clear();
  }//clear

  public Set<String> keySet() {
    return m_Cache.keySet();
  }//keySet

  public Collection<String> values() {
    return m_Cache.values();
  }//values

  public Set<Entry<String, String>> entrySet() {
    return m_Cache.entrySet();
  }//entrySet

}//class TransformCache
