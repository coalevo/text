/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.text.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * Provides standard ANSI Terminal colors and styles.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ANSIHelper {

  //Constants
  private static final int S = 30; //black
  private static final int s = 40;
  private static final int R = 31; //red
  private static final int r = 41;
  private static final int G = 32; //green
  private static final int g = 42;
  private static final int Y = 33; //yellow
  private static final int y = 43;
  private static final int B = 34; //blue
  private static final int b = 44;
  private static final int M = 35; //magenta
  private static final int m = 45;
  private static final int C = 36; //cyan
  private static final int c = 46;
  private static final int W = 37; //white
  private static final int w = 47;

  private static final int f = 1;  /*bold*/

  private static final int d = 22; /*!bold*/ //normal color or normal intensity
  private static final int i = 3;  /*italic*/
  private static final int j = 23; /*!italic*/
  private static final int u = 4;  /*underlined*/
  private static final int v = 24; /*!underlined*/
  private static final int e = 5;  /*blink*/
  private static final int n = 25; /*steady = !blink*/
  private static final int h = 8;  /*hide = concealed characters*/
  private static final int a = 0;  /*all out*/

  private static int[] c_ColorMapping;    //translation table
  private static Map<String, Character> c_ColorStringMapping;

  /**
   * Defines the markup representation of the color
   * black.
   */
  public static final Character BLACK = new Character('S');


  /**
   * Defines the markup representation of the color
   * red.
   */
  public static final Character RED = new Character('R');

  /**
   * Defines the markup representation of the color
   * green.
   */
  public static final Character GREEN = new Character('G');

  /**
   * Defines the markup representation of the color
   * yellow.
   */
  public static final Character YELLOW = new Character('Y');

  /**
   * Defines the markup representation of the color
   * blue.
   */
  public static final Character BLUE = new Character('B');

  /**
   * Defines the markup representation of the color magenta.
   */
  public static final Character MAGENTA = new Character('M');

  /**
   * Defines the markup representation of the color
   * cyan.
   */
  public static final Character CYAN = new Character('C');

  /**
   * Defines the markup representation of the color
   * white.
   */
  public static final Character WHITE = new Character('W');


  /**
   * Defines the markup representation of the activator
   * for style bold (normally represented by high intensity).
   */
  public static final Character BOLD = new Character('f');

  /**
   * Defines the markup representation of the deactivator
   * for style bold.
   */
  private static final Character BOLD_OFF = new Character('d'); //normal color or normal intensity

  /**
   * Defines the markup representation of the activator
   * for style italic.
   */
  public static final Character ITALIC = new Character('i');

  /**
   * Defines the markup representation of the deactivator
   * for style italic.
   */
  private static final Character ITALIC_OFF = new Character('j');

  /**
   * Defines the markup representation of the activator
   * for style underlined.
   */
  public static final Character UNDERLINED = new Character('u');

  /**
   * Defines the markup representation of the deactivator
   * for style underlined.
   */
  private static final Character UNDERLINED_OFF = new Character('v');

  /**
   * Defines the markup representation of the activator
   * for style blinking.
   */
  public static final Character BLINK = new Character('e');

  /**
   * Defines the markup representation of the deactivator
   * for style blinking (i.e. steady)
   */
  private static final Character BLINK_OFF = new Character('n');

  /**
   * Defines the markup representation of the graphics rendition
   * reset.<br>
   * It will reset all set colors and styles.
   */
  private static final Character RESET_ALL = new Character('a');

  private static final String EMPTY_STRING = "";

  /**
   * Constructs a Colorizer with its translation table.
   */
  static {

    c_ColorMapping = new int[128];

    c_ColorMapping[83] = S;
    c_ColorMapping[82] = R;
    c_ColorMapping[71] = G;
    c_ColorMapping[89] = Y;
    c_ColorMapping[66] = B;
    c_ColorMapping[77] = M;
    c_ColorMapping[67] = C;
    c_ColorMapping[87] = W;

    c_ColorMapping[115] = s;
    c_ColorMapping[114] = r;
    c_ColorMapping[103] = g;
    c_ColorMapping[121] = y;
    c_ColorMapping[98] = b;
    c_ColorMapping[109] = m;
    c_ColorMapping[99] = c;
    c_ColorMapping[119] = w;

    c_ColorMapping[102] = f;
    c_ColorMapping[100] = d;
    c_ColorMapping[105] = i;
    c_ColorMapping[106] = j;
    c_ColorMapping[117] = u;
    c_ColorMapping[118] = v;
    c_ColorMapping[101] = e;
    c_ColorMapping[110] = n;
    c_ColorMapping[104] = h;
    c_ColorMapping[97] = a;

    c_ColorStringMapping = new HashMap<String, Character>(10);
    c_ColorStringMapping.put("red", RED);
    c_ColorStringMapping.put("white", WHITE);
    c_ColorStringMapping.put("black", BLACK);
    c_ColorStringMapping.put("blue", BLUE);
    c_ColorStringMapping.put("green", GREEN);
    c_ColorStringMapping.put("cyan", CYAN);
    c_ColorStringMapping.put("magenta", MAGENTA);
    c_ColorStringMapping.put("yellow", YELLOW);

  }//static initializer


  private Character m_FGColor = WHITE;
  private Character m_BGColor = BLACK;
  private Stack<Character> m_FGColors;
  private Stack<Character> m_BGColors;
  private Stack<Character> m_Styles;

  public ANSIHelper() {
    m_FGColors = new Stack<Character>();
    m_BGColors = new Stack<Character>();
    m_Styles = new Stack<Character>();
  }//ANSIHelper

  public String resetAll() {
    final StringBuilder sbuf = new StringBuilder();
    sbuf.append((char) 27);
    sbuf.append((char) 91);
    sbuf.append(c_ColorMapping[RESET_ALL.charValue()]);
    sbuf.append((char) 109);
    return sbuf.toString();
  }//resetAll

  public String startFgColor(Character color) {
    if (color == null) {
      return EMPTY_STRING;
    }
    m_FGColors.push(color);
    final StringBuilder sbuf = new StringBuilder();
    sbuf.append((char) 27);
    sbuf.append((char) 91);
    sbuf.append(c_ColorMapping[color.charValue()]);
    sbuf.append((char) 109);
    return sbuf.toString();
  }//startFgColor

  public String endFgColor() {
    final int stacksize = m_FGColors.size();
    Character c = m_FGColor;
    if (stacksize >= 1) {
      m_FGColors.pop();
    }
    if (stacksize > 1) {
      c = (Character) m_FGColors.peek();
    }

    final StringBuilder sbuf = new StringBuilder();
    sbuf.append((char) 27);
    sbuf.append((char) 91);
    sbuf.append(c_ColorMapping[c.charValue()]);
    sbuf.append((char) 109);
    return sbuf.toString();
  }//endFgColor

  public String startBgColor(Character color) {
    if (color == null) {
      return EMPTY_STRING;
    }
    m_BGColors.push(color);
    final StringBuilder sbuf = new StringBuilder();
    sbuf.append((char) 27);
    sbuf.append((char) 91);
    sbuf.append(c_ColorMapping[color.charValue() + 32]);
    sbuf.append((char) 109);
    return sbuf.toString();
  }//startBgColor

  public String endBgColor() {
    final int stacksize = m_BGColors.size();
    Character c = m_BGColor;
    if (stacksize >= 1) {
      m_BGColors.pop();
    }
    if (stacksize > 1) {
      c = (Character) m_BGColors.peek();
    }
    final StringBuilder sbuf = new StringBuilder();
    sbuf.append((char) 27);
    sbuf.append((char) 91);
    sbuf.append(c_ColorMapping[c.charValue() + 32]);
    sbuf.append((char) 109);
    return sbuf.toString();
  }//endBgColor


  public String startStyle(Character style) {
    if (style == null) {
      return EMPTY_STRING;
    }
    m_Styles.push(style);
    final StringBuilder sbuf = new StringBuilder();
    sbuf.append((char) 27);
    sbuf.append((char) 91);
    sbuf.append(c_ColorMapping[style.charValue()]);
    sbuf.append((char) 109);
    return sbuf.toString();
  }//startBgColor

  public String endStyle() {
    if (m_Styles.isEmpty()) {
      return "";
    } else {
      Character c = (Character) m_Styles.pop();
      if (c.equals(ITALIC)) {
        c = ITALIC_OFF;
      } else if (c.equals(BOLD)) {
        c = BOLD_OFF;
      } else if (c.equals(BLINK)) {
        c = BLINK_OFF;
      } else if (c.equals(UNDERLINED)) {
        c = UNDERLINED_OFF;
      }
      final StringBuilder sbuf = new StringBuilder();
      sbuf.append((char) 27);
      sbuf.append((char) 91);
      sbuf.append(c_ColorMapping[c.charValue()]);
      sbuf.append((char) 109);
      return sbuf.toString();
    }
  }//endBgColor

  public static final Character getColor(String color) {
    return c_ColorStringMapping.get(color);
  }//getColor

  /**
   * Returns the length of the visible string calculated
   * from the internal marked-up string passed as parameter.
   *
   * @param str String with internal color/style markups.
   * @return long Representing the length of the visible string..
   */
  public static int getVisibleLength(String str) {
    int counter = 0;
    int parsecursor = 0;
    int foundcursor = 0;

    boolean done = false;

    while (!done) {
      foundcursor = str.indexOf(27, parsecursor);
      if (foundcursor != -1) {
        //always should find m
        int nextidx = str.indexOf(109, foundcursor) + 1;
        //increment counter
        counter += nextidx - foundcursor;
        //parseon from the next char
        parsecursor = nextidx;

        //System.out.println("lidx=" +foundcursor + "::nidx=" + nextidx + "::counter=" + counter+"::pidx=" + parsecursor);

      } else {
        done = true;
      }
    }
    //System.out.println("strlen=" + str.length() + "::counter=" + counter);
    return (str.length() - counter);
  }//getVisibleLength

  public static void main(String[] args) {
    ANSIHelper h = new ANSIHelper();
    StringBuilder sbuf = new StringBuilder();
    sbuf.append(h.startFgColor(YELLOW));
    sbuf.append(h.startBgColor(BLUE));
    sbuf.append("Test ");
    sbuf.append(h.startFgColor(RED));
    sbuf.append("me");
    sbuf.append(h.endFgColor());
    sbuf.append("!");
    sbuf.append(h.endBgColor());
    sbuf.append(h.endFgColor());
    System.out.println(sbuf.toString());
  }


}//class ANSIHelper
