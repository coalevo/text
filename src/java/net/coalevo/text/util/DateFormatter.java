/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.text.util;

import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Provides a pooled date format helper class.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class DateFormatter {

  private static final Logger log = LoggerFactory.getLogger(DateFormatter.class);
  private static DateFormatter c_ISOInstance;
  private String m_Pattern;
  private GenericObjectPool m_FormatterPool;
  private TimeZone m_TimeZone = TimeZone.getDefault();

  public DateFormatter(String pattern, int poolsize) {
    m_Pattern = pattern;
    GenericObjectPool.Config poolcfg = new GenericObjectPool.Config();
    poolcfg.maxActive = poolsize;
    poolcfg.maxIdle = poolsize;
    poolcfg.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_BLOCK;
    poolcfg.testOnBorrow = true;
    m_FormatterPool = new GenericObjectPool(new DateFormatFactory(), poolcfg);
  }//DateFormatter

  public DateFormatter(String pattern, String timezone, int poolsize) {
    m_Pattern = pattern;
    GenericObjectPool.Config poolcfg = new GenericObjectPool.Config();
    poolcfg.maxActive = poolsize;
    poolcfg.maxIdle = poolsize;
    poolcfg.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_BLOCK;
    poolcfg.testOnBorrow = true;
    m_TimeZone = TimeZone.getTimeZone(timezone);
    m_FormatterPool = new GenericObjectPool(new DateFormatFactory(), poolcfg);
  }//DateFormatter

  protected DateFormatter() {
    m_Pattern = "yyyy-MM-dd HH:mm:ss";
    GenericObjectPool.Config poolcfg = new GenericObjectPool.Config();
    poolcfg.maxActive = 10;
    poolcfg.maxIdle = 5;
    poolcfg.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_GROW;
    poolcfg.testOnBorrow = true;
    m_FormatterPool = new GenericObjectPool(new DateFormatFactory(), poolcfg);
  }//DateFormatter

  /**
   * Returns the pattern used in this instance.
   *
   * @return the pattern string used for formatting dates.
   */
  public String getPattern() {
    return m_Pattern;
  }//getPattern

  /**
   * Returns the TimeZone used in this instance.
   *
   * @return the TimeZone.
   */
  public TimeZone getTimeZone() {
    return m_TimeZone;
  }//getTimeZone

  /**
   * Convenience method that formats the actual date.
   *
   * @return the date formatted to a String using the pattern of this instance.
   */
  public String format() {
    return format(null);
  }//format

  /**
   * Formats the given date using the pattern of this instance.
   * <p/>
   * The formatting will be done using a format from the pool.
   * If the given date is null, then the actual date will be formatted
   * generating a <tt>new Date()</tt> instance.
   * </p>
   *
   * @param d a <tt>Date</tt>.
   * @return the date formatted to a String using the pattern of this instance.
   */
  public String format(Date d) {
    if (d == null) {
      d = new Date();
    }
    DateFormat df = null;
    try {
      df = (DateFormat) m_FormatterPool.borrowObject();
      return df.format(d);
    } catch (Exception ex) {
      log.error("format()", ex);
    } finally {
      try {
        m_FormatterPool.returnObject(df);
      } catch (Exception e) {
        log.error("format()", e);
      }
    }
    //fallback to some default format
    return d.toString();
  }//format

  /**
   * Convenience method that formats the given UTC millis into a
   * date.
   *
   * @param l the date as long.
   * @return the date formatted to a String using the pattern of this instance.
   */
  public String format(long l) {
    return format(new Date(l));
  }//format

  public Date parse(String str) throws ParseException {
    DateFormat df = null;
    try {
      df = (DateFormat) m_FormatterPool.borrowObject();
      return df.parse(str);
    } catch (Exception ex) {
      log.error("parse()", ex);
    } finally {
      try {
        m_FormatterPool.returnObject(df);
      } catch (Exception e) {
        log.error("format()", e);
      }
    }
    return new Date();
  }//format

  public void setPoolsize(int size) {
    m_FormatterPool.setMaxActive(size);
    m_FormatterPool.setMaxIdle(size);
  }//setPoolsize

  private class DateFormatFactory
      extends BasePoolableObjectFactory {

    public DateFormatFactory() {

    }//constructor

    public Object makeObject() throws Exception {
      SimpleDateFormat df = new SimpleDateFormat(m_Pattern);
      df.setTimeZone(m_TimeZone);
      return df;
    }//makeObject

  }//DateFormatFactory

  public static DateFormatter getISOInstance() {
    if(c_ISOInstance == null) {
      c_ISOInstance = new DateFormatter();
    }
    return c_ISOInstance;
  }//getISOInstance

}//class DateFormatter
