/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.text.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * Provides TelnetD specific colors and styles.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class TelnetdHelper {

  private static Map<String, Character> c_ColorStringMapping;
  private static Map<String, Character> c_StyleStringMapping;

  /**
   * Defines the internal marker string
   * for style/color markups.
   */
  public static final char INTERNAL_MARKER = '\001';

  /**
   * Defines the markup representation of the color
   * black.
   */
  public static final Character BLACK = new Character('S');


  /**
   * Defines the markup representation of the color
   * red.
   */
  public static final Character RED = new Character('R');

  /**
   * Defines the markup representation of the color
   * green.
   */
  public static final Character GREEN = new Character('G');

  /**
   * Defines the markup representation of the color
   * yellow.
   */
  public static final Character YELLOW = new Character('Y');

  /**
   * Defines the markup representation of the color
   * blue.
   */
  public static final Character BLUE = new Character('B');

  /**
   * Defines the markup representation of the color magenta.
   */
  public static final Character MAGENTA = new Character('M');

  /**
   * Defines the markup representation of the color
   * cyan.
   */
  public static final Character CYAN = new Character('C');

  /**
   * Defines the markup representation of the color
   * white.
   */
  public static final Character WHITE = new Character('W');


  /**
   * Defines the markup representation of the activator
   * for style bold (normally represented by high intensity).
   */
  public static final Character BOLD = new Character('f');

  /**
   * Defines the markup representation of the deactivator
   * for style bold.
   */
  private static final Character BOLD_OFF = new Character('d'); //normal color or normal intensity

  /**
   * Defines the markup representation of the activator
   * for style italic.
   */
  public static final Character ITALIC = new Character('i');

  /**
   * Defines the markup representation of the deactivator
   * for style italic.
   */
  private static final Character ITALIC_OFF = new Character('j');

  /**
   * Defines the markup representation of the activator
   * for style underlined.
   */
  public static final Character UNDERLINED = new Character('u');

  /**
   * Defines the markup representation of the deactivator
   * for style underlined.
   */
  private static final Character UNDERLINED_OFF = new Character('v');

  /**
   * Defines the markup representation of the activator
   * for style blinking.
   */
  public static final Character BLINK = new Character('e');

  /**
   * Defines the markup representation of the deactivator
   * for style blinking (i.e. steady)
   */
  private static final Character BLINK_OFF = new Character('n');

  /**
   * Defines the markup representation of the graphics rendition
   * reset.<br>
   * It will reset all set colors and styles.
   */
  private static final Character RESET_ALL = new Character('a');

  private static final String EMPTY_STRING = "";

  /**
   * Constructs a Colorizer with its translation table.
   */
  static {
    c_ColorStringMapping = new HashMap<String, Character>(10);
    c_ColorStringMapping.put("red", RED);
    c_ColorStringMapping.put("white", WHITE);
    c_ColorStringMapping.put("black", BLACK);
    c_ColorStringMapping.put("blue", BLUE);
    c_ColorStringMapping.put("green", GREEN);
    c_ColorStringMapping.put("cyan", CYAN);
    c_ColorStringMapping.put("magenta", MAGENTA);
    c_ColorStringMapping.put("yellow", YELLOW);

    c_StyleStringMapping = new HashMap<String, Character>(10);
    c_StyleStringMapping.put("italic", ITALIC);
    c_StyleStringMapping.put("blink", BLINK);
    c_StyleStringMapping.put("underlined", UNDERLINED);
    c_StyleStringMapping.put("bold", BOLD);

  }//static initializer


  private Character m_FGColor = GREEN;
  private Character m_BGColor = BLACK;
  private Stack<Character> m_FGColors;
  private Stack<Character> m_BGColors;
  private Stack<Character> m_Styles;

  public TelnetdHelper() {
    m_FGColors = new Stack<Character>();
    m_BGColors = new Stack<Character>();
    m_Styles = new Stack<Character>();
  }//ANSIHelper

  public void setDefaultFgColor(Character color) {
    m_FGColor = color;
  }//setDefaultFgColor

  public void setDefaultBgColor(Character color) {
    m_BGColor = color;
  }//setDefaultBgColor

  public String startFgColor(Character color) {
    if (color == null) {
      return EMPTY_STRING;
    }
    m_FGColors.push(color);
    final StringBuilder sbuf = new StringBuilder();
    sbuf.append(INTERNAL_MARKER);
    sbuf.append(color);
    return sbuf.toString();
  }//startFgColor

  public String endFgColor() {
    final int stacksize = m_FGColors.size();
    Character color = m_FGColor;
    if (stacksize >= 1) {
      m_FGColors.pop();
    }
    if (stacksize > 1) {
      color = (Character) m_FGColors.peek();
    }

    final StringBuilder sbuf = new StringBuilder();
    sbuf.append(INTERNAL_MARKER);
    sbuf.append(color);
    return sbuf.toString();
  }//endFgColor

  public String startBgColor(Character color) {
    if (color == null) {
      return EMPTY_STRING;
    }
    m_BGColors.push(color);
    final StringBuilder sbuf = new StringBuilder();
    sbuf.append(INTERNAL_MARKER);
    sbuf.append(Character.toLowerCase(color.charValue()));
    return sbuf.toString();
  }//startBgColor

  public String endBgColor() {
    final int stacksize = m_BGColors.size();
    Character color = m_BGColor;
    if (stacksize >= 1) {
      m_BGColors.pop();
    }
    if (stacksize > 1) {
      color = (Character) m_BGColors.peek();
    }
    final StringBuilder sbuf = new StringBuilder();
    sbuf.append(INTERNAL_MARKER);
    sbuf.append(Character.toLowerCase(color.charValue()));
    return sbuf.toString();
  }//endBgColor


  public String startStyle(Character style) {
    if (style == null) {
      return EMPTY_STRING;
    }
    m_Styles.push(style);
    final StringBuilder sbuf = new StringBuilder();
    sbuf.append(INTERNAL_MARKER);
    sbuf.append(Character.toLowerCase(style.charValue()));
    return sbuf.toString();
  }//startBgColor

  public String endStyle() {
    if (m_Styles.isEmpty()) {
      return "";
    } else {
      Character c = (Character) m_Styles.pop();
      if (c.equals(ITALIC)) {
        c = ITALIC_OFF;
      } else if (c.equals(BOLD)) {
        c = BOLD_OFF;
      } else if (c.equals(BLINK)) {
        c = BLINK_OFF;
      } else if (c.equals(UNDERLINED)) {
        c = UNDERLINED_OFF;
      }
      final StringBuilder sbuf = new StringBuilder();
      sbuf.append(INTERNAL_MARKER);
      sbuf.append(Character.toLowerCase(c.charValue()));
      return sbuf.toString();
    }
  }//endBgColor

  public final Character getColor(String color) {
    Object o = c_ColorStringMapping.get(color.toLowerCase());
    if (o == null) {
      return m_FGColor;
    } else {
      return (Character) o;
    }
  }

  public final Character getStyle(String style) {
    Object o = c_StyleStringMapping.get(style.toLowerCase());
    if (o == null) {
      return RESET_ALL;
    } else {
      return (Character) o;
    }
  }//getStyle

  /**
   * Returns the length of the visible string calculated
   * from the internal marked-up string passed as parameter.
   *
   * @param str String with internal color/style markups.
   * @return long Representing the length of the visible string..
   */
  public static int getVisibleLength(String str) {
    int counter = 0;
    int parsecursor = 0;
    int foundcursor = 0;

    boolean done = false;

    while (!done) {
      foundcursor = str.indexOf(MARKER_CODE, parsecursor);
      if (foundcursor != -1) {
        //increment counter
        counter++;
        //parseon from the next char
        parsecursor = foundcursor + 1;
      } else {
        done = true;
      }
    }

    return (str.length() - (counter * 2));
  }//getVisibleLength


  /**
   * Defines the internal marker character code.
   */
  public static final int MARKER_CODE = 1;


}//class TelnetdHelper
