/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.text.util;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

/**
 * Utility class for handling tags.
 * <p/>
 * Todo: Make operation similar to other tag facilities (e.g. , or space or "")
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class TagUtility {

  private TagUtility() {
  }

  ; //non instantiable

  public static final Set<String> toSet(String tags) {
    Set<String> tagset = new TreeSet<String>();

    if (tags != null && tags.length() > 0) {
      String[] ts = tags.split(",");
      for (int i = 0; i < ts.length; i++) {
        tagset.add(ts[i].trim());
      }
    }
    return tagset;
  }//toSet

  public static final String fromSet(Set<String> tagset) {

    StringBuilder sb = new StringBuilder();
    if (tagset != null && tagset.size() > 0) {
      for (Iterator<String> iterator = tagset.iterator(); iterator.hasNext();) {
        sb.append(iterator.next());
        if (iterator.hasNext()) {
          sb.append(',');
        }
      }
    }
    return sb.toString();
  }//fromSet

}//class TagUtility