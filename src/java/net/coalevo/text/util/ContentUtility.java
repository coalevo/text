/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.text.util;

/**
 * Provides methods for handling string content.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ContentUtility {

  /**
   * Not constructable utility class.
   */
  private ContentUtility() {
  }

  public static String translateColorCodes(String str) {
    return str.replaceAll("@", "\001");
  }//translateColorCodes

  public static String adjustLength(String str, int len) {
    int sl = (int) TelnetdHelper.getVisibleLength(str);
    if (len < sl) {
      StringBuilder sb = new StringBuilder();
      int i = 0;
      do {
        char c = str.charAt(i++);
        if (c == '\001') {
          sb.append(c).append(str.charAt(i++));
        } else {
          sb.append(c);
          len--;
        }
      } while (len > 0);
      sb.append("\001a");
      return sb.toString();
    }
    if (len > sl) {
      int pad = len - sl;
      StringBuilder sb = new StringBuilder(str);
      for (int i = 0; i < pad; i++) {
        sb.append(' ');
      }
      return sb.toString();
    }
    return str;
  }//adjustLength

  public static final String padding(String str, int len, char pad) {
    final StringBuilder sb = new StringBuilder(len);
    if (str != null) {
      sb.append(str);
    }
    while (sb.length() < len) {
      sb.append(pad);
    }
    return sb.toString();
  }//padding

  /**
   * Filters emails transforming them into a string that
   * represents an email, but is not parsable for spammer
   * attacks.
   *
   * @param str the <tt>String</tt> to be filtered.
   * @return the <tt>String</tt> as non parsable but recognizable email.
   */
  public static String transformEmail(String str) {
    return (str == null) ? EMPTY : str.replaceAll("@", " at ");
  }//transformEmail

  /**
   * Filters names into a string that
   * represents an better filename.
   *
   * @param str the <tt>String</tt> to be filtered.
   * @return the <tt>String</tt> as filename.
   */
  public static String transformToFilename(String str) {
    return (str == null) ? EMPTY : str.trim().replaceAll(" ", "_");
  }//transformToFilename

  /**
   * Filters non valid UTF-8 characters
   * @param in input.
   * @return the stripped output.
   */
  public static String stripNonValidCharacters(String in) {
    StringBuilder out = new StringBuilder(); // Used to hold the output.
    char current; // Used to reference the current character.
    if (in == null || ("".equals(in))) return ""; // vacancy test.
    for (int i = 0; i < in.length(); i++) {
      current = in.charAt(i); // NOTE: No IndexOutOfBoundsException caught here; it should not happen.
      if (isValidChar(current)) {
        out.append(current);
      }
    }
    return out.toString();
  }//stripNonValidXMLCharacters

  /**
   * An empty String.
   */
  private static String EMPTY = "";

  /**
   * A space character for padding.
   */
  public static final char SPACE_PAD = ' ';

  private static boolean isValidChar(int i) {
    return ((i == 0x9) ||
        (i == 0xA) ||
        (i == 0xD) ||
        ((i >= 0x20) && (i <= 0xD7FF)) ||
        ((i >= 0xE000) && (i <= 0xFFFD)) ||
        ((i >= 0x10000) && (i <= 0x10FFFF)));
  }//isValidChar

}//class ContentUtility
