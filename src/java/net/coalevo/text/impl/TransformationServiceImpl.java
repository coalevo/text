/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.text.impl;

import net.coalevo.text.model.*;
import net.coalevo.text.service.TransformationService;
import org.osgi.framework.BundleContext;

import java.util.Set;

/**
 * Provides an implementation of {@link TransformationService}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class TransformationServiceImpl
    implements TransformationService {

  private TransformerManager m_TransformerManager;

  public boolean activate(BundleContext bc) {
    try {
      //1. Prepare manager
      m_TransformerManager = new TransformerManager();
      m_TransformerManager.activate(bc);

    } catch (Exception ex) {
      return false;
    }
    return true;
  }//activate

  public void deactivate() {
    m_TransformerManager.deactivate();
  }//activate

  public String transform(String inf, String outf, String text)
      throws UnsupportedTransformException, TransformationException {
    Transformer t = m_TransformerManager.get(inf, outf);
    return t.transform(text);
  }//transform

  public Set<String> getInputFormats() {
    return m_TransformerManager.getInputFormats();
  }//getInputFormats

  public Set<String> getInputFormats(String outf)
      throws UnsupportedOutputFormatException {
    return m_TransformerManager.getInputFormats(outf);
  }//getInputFormats

  public Set<String> getOutputFormats() {
    return m_TransformerManager.getOutputFormats();
  }//getOutputFormats

  public Set<String> getOutputFormats(String inf)
      throws UnsupportedInputFormatException {
    return m_TransformerManager.getOutputFormats(inf);
  }//getOutputFormats

}//class TransformationServiceImpl
