/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.text.impl;

import net.coalevo.text.model.BaseTransformer;
import net.coalevo.text.model.TransformationException;
import net.coalevo.text.model.Transformer;

/**
 * Provides a {@link Transformer} implementation that
 * strips color markups (@.).
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class Color2PlainTransformer
    extends BaseTransformer {

  public Color2PlainTransformer() {
    super("colors", "plain");
  }//Color2PlainTransformer

  public String transform(String input) throws TransformationException {
    return input.replaceAll("@.", "");
  }//transform

}//class Color2PlainTransformer
