/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.text.impl;

import net.coalevo.text.model.Transformer;
import net.coalevo.text.model.UnsupportedInputFormatException;
import net.coalevo.text.model.UnsupportedOutputFormatException;
import net.coalevo.text.model.UnsupportedTransformException;
import org.osgi.framework.*;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This is the manager for {@link Transformer} instances, which
 * are handled according to the OSGi white-board model.
 * All registered instances of the {@link Transformer} class will be
 * handled here to make them available to the {@link TransformationServiceImpl}.
 * <p/>
 * TODO: The tranformers should be accumulated into a map, and then using Dijkstra's
 * Shortest Path, chained into indirect transformers. The weight could be 1 on all
 * connections, or the number of passes a transformation requires in itself.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class TransformerManager {

  private static Logger log = LoggerFactory.getLogger(TransformerManager.class);
  private BundleContext m_BundleContext;
  private Map<String, Transformer> m_Transformers;
  private Set<String> m_InputFormats;
  private Set<String> m_OutputFormats;
  private Map<String, Set<String>> m_OutputsForInput;
  private Map<String, Set<String>> m_InputsForOutput;

  private TransformerListener m_TransformerListener;


  public TransformerManager() {
    m_Transformers = new ConcurrentHashMap<String, Transformer>();
    m_InputFormats = new HashSet<String>();
    m_OutputFormats = new HashSet<String>();
    m_OutputsForInput = new HashMap<String, Set<String>>();
    m_InputsForOutput = new HashMap<String, Set<String>>();
  }//TransformerManager

  /**
   * Activates this <tt>TransformerManager</tt>.
   * The logic will automatically detect all {@link Transformer}
   * class objects in the container, whether registered before or after
   * the activation (i.e. white board model implementation).
   *
   * @param bc the <tt>BundleContext</tt>.
   */
  public void activate(BundleContext bc) {
    //get the context
    m_BundleContext = bc;
    //prepare listener
    m_TransformerListener = new TransformerListener();
    //prepare the filter
    String filter = "(objectclass=" + Transformer.class.getName() + ")";

    try {
      //add the listener to the bundle context.
      bc.addServiceListener(m_TransformerListener, filter);
      //ensure that already registered Provider instances are registered with
      //the manager
      ServiceReference[] srl = bc.getServiceReferences(null, filter);
      for (int i = 0; srl != null && i < srl.length; i++) {
        m_TransformerListener.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, srl[i]));
      }
    } catch (InvalidSyntaxException ex) {
      log.error("activate()", ex);
    }
  }//activate

  /**
   * Deactivates this <tt>ShellCommandProviderManagerImpl</tt>.
   * The logic will remove the listener and release all
   * references.
   */
  public void deactivate() {
    //remove the listener
    m_BundleContext.removeServiceListener(m_TransformerListener);
    m_Transformers.clear();
    m_InputFormats.clear();
    m_OutputFormats.clear();
    m_InputsForOutput.clear();
    m_OutputsForInput.clear();

    //null out the references
    m_Transformers = null;
    m_InputFormats = null;
    m_OutputFormats = null;
    m_InputsForOutput = null;
    m_OutputsForInput = null;
    m_TransformerListener = null;
    m_BundleContext = null;
  }//deactivate

  public boolean register(Transformer t) {
    final String id = t.getIdentifier();
    if (m_Transformers.containsKey(id)) {
      Transformer tl = m_Transformers.get(id);
      if (t.getNumberOfPasses() < tl.getNumberOfPasses()) {
        //replace
        m_Transformers.put(id, t);
        //add formats
        m_InputFormats.add(t.getInputFormat());
        m_OutputFormats.add(t.getOutputFormat());
      }
      return false;
    } else {
      m_Transformers.put(id, t);
      //add formats
      m_InputFormats.add(t.getInputFormat());
      m_OutputFormats.add(t.getOutputFormat());
      //Inputs for Outputformat
      Set<String> inputs = m_InputsForOutput.get(t.getOutputFormat());
      if (inputs == null) {
        inputs = new HashSet<String>();
        m_InputsForOutput.put(t.getOutputFormat(), inputs);
      }
      inputs.add(t.getInputFormat());

      //Outputs for Inputformat
      Set<String> outputs = m_OutputsForInput.get(t.getInputFormat());
      if (outputs == null) {
        outputs = new HashSet<String>();
        m_OutputsForInput.put(t.getInputFormat(), outputs);
      }
      outputs.add(t.getOutputFormat());

      log.info("Registered Transformer " + id);
      return true;
    }
  }//register

  public boolean unregister(Transformer t) {
    final String id = t.getIdentifier();
    if (!m_Transformers.containsKey(id)) {
      return false;
    } else {
      removeFormats(m_Transformers.remove(id));
      log.info("Unregistered Transformer " + t);
      return true;
    }

  }//unregister

  public Transformer get(String inf, String outf) {
    final String id = new StringBuilder(inf).append("=>").append(outf).toString();
    ;
    Object o = m_Transformers.get(id);
    if (o != null) {
      return (Transformer) o;
    } else {
      if (!m_InputFormats.contains(inf)) {
        throw new UnsupportedInputFormatException(inf);
      }
      if (!m_OutputFormats.contains(outf)) {
        throw new UnsupportedOutputFormatException(outf);
      }
      throw new UnsupportedTransformException();
    }
  }//get

  public boolean isAvailable(String inf, String outf) {
    final String id = new StringBuilder(inf).append("=>").append(outf).toString();
    return m_Transformers.containsKey(id);
  }//isAvailable

  public Set<String> getInputFormats() {
    return Collections.unmodifiableSet(m_InputFormats);
  }//getInputFormats

  public Set<String> getInputFormats(String outf) {
    return Collections.unmodifiableSet(m_InputsForOutput.get(outf));
  }//getInputFormats

  public Set<String> getOutputFormats() {
    return Collections.unmodifiableSet(m_OutputFormats);
  }//getOutputFormats

  public Set<String> getOutputFormats(String inf) {
    return Collections.unmodifiableSet(m_OutputsForInput.get(inf));
  }//getOutputFormats

  private void removeFormats(Transformer rt) {
    String inf = rt.getInputFormat();
    String outf = rt.getOutputFormat();
    boolean infound = false;
    boolean outfound = false;

    //safeguards
    if(m_Transformers == null) {
      return;
    }
    Collection<Transformer> values = m_Transformers.values();
    if(values == null) {
      return;
    }
    //1. Simple format checkers
    for (Iterator<Transformer> iter = values.iterator(); iter.hasNext();) {
      Transformer t = iter.next();
      //Concious comparison, must be the same instance, not equal.
      if (rt == t) {
        continue;  //skip it
      }

      if (inf.equals(t.getInputFormat())) {
        infound = true;
      }
      if (outf.equals(t.getOutputFormat())) {
        outfound = true;
      }
      if(infound && outfound) {
        break;
      }
    }

    if (!infound) {
      m_InputFormats.remove(inf);
    }
    if (!outfound) {
      m_OutputFormats.remove(outf);
    }

    //2. Inputs for Outputs
    Set<String> inputs = m_InputsForOutput.get(outf);
    if (inputs != null && !infound) {
      inputs.remove(inf);
    }
    //2. Outputs for Inputs
    Set<String> outputs = m_OutputsForInput.get(inf);
    if (outputs != null && !outfound) {
      outputs.remove(outf);
    }

  }//removeFormats

  private class TransformerListener
      implements ServiceListener {

    public void serviceChanged(ServiceEvent ev) {
      ServiceReference sr = ev.getServiceReference();
      Object o = null;
      switch (ev.getType()) {
        case ServiceEvent.REGISTERED:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            log.error("ServiceListener:serviceChanged:registered:null");
          } else if (!(o instanceof Transformer)) {
            log.error("ServiceListener:serviceChanged:registered:Reference not a Transformer instance.");
          } else {
            register((Transformer) o);
          }
          break;
        case ServiceEvent.UNREGISTERING:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            log.error("ServiceListener:serviceChanged:unregistering:null");
          } else if (!(o instanceof Transformer)) {
            log.error("ServiceListener:serviceChanged:unregistering:Reference not a ShellService instance.");
          } else {
            unregister((Transformer) o);
          }
          break;
      }
    }
  }//inner class TransformerListener


}//class TransformerManager
