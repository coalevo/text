/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.text.impl;

import net.coalevo.text.model.Transformer;
import net.coalevo.text.service.TransformationService;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides a <tt>BundleActivator</tt> implementation for
 * the messaging service bundle.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Activator
    implements BundleActivator {

  private static Logger log = LoggerFactory.getLogger(Activator.class);
  private static TransformationServiceImpl c_TransformationService;

  private Thread m_StartThread;

  public void start(final BundleContext bundleContext)
      throws Exception {
    if(m_StartThread!=null && m_StartThread.isAlive()) {
      throw new Exception();
    }
    m_StartThread = new Thread(
        new Runnable() {
          public void run() {
            try {
              //1. Setup service
              c_TransformationService = new TransformationServiceImpl();

              if (!c_TransformationService.activate(bundleContext)) {
                log.error("start()");
                return;
              }
              //2. Register the service
              bundleContext.registerService(
                  TransformationService.class.getName(),
                  c_TransformationService,
                  null);

              //3. Register locally implemented transformers
              bundleContext.registerService(
                  Transformer.class.getName(),
                  new Color2PlainTransformer(),
                  null);
              bundleContext.registerService(
                  Transformer.class.getName(),
                  new Color2TelnetDTransformer(),
                  null);

            } catch (Exception ex) {
              log.error("start(BundleContext)",ex);
            }
          }//run
        }//Runnable
    );//Thread
    m_StartThread.setContextClassLoader(getClass().getClassLoader());
    m_StartThread.start();
  }//start

  public void stop(BundleContext bundleContext) throws Exception {
    //wait start
    if(m_StartThread != null && m_StartThread.isAlive()) {
      m_StartThread.join();
      m_StartThread = null;
    }
    try {
      if (c_TransformationService != null) {
        c_TransformationService.deactivate();
        c_TransformationService = null;
      }
    } catch (Exception ex) {
      log.error("stop()", ex);
    }
  }//stop

}//class Activator
